
# Jenkins on AWS Test cannot connect to Azure Docker Swarm
ssh ubuntu@jenkins-test
export DOCKER_HOST=tcp://docker-test-a.northeurope.cloudapp.azure.com:2376
export DOCKER_TLS_VERIFY=1
export DOCKER_CERT_PATH=./docker-a-certs
docker node ls
exit

# Find reference to AWS Test NAT gateway IP
cd ~/repos/ds-infra/terraform/aws/environment/test
terraform output | grep nat-gateway

# Create firewall rule on Azure to allow connection from AWS Test NAT gateway
cd ~/repos/ds-infra/terraform/azure/environment/test/base
ls -l
mg public-subnet.tf

resource "azurerm_network_security_rule" "public-allow-jenkins-deploy" {
  name = "allow-jenkins-deploy_nsr"
  priority = 900
  direction = "inbound"
  access = "allow"
  protocol = "tcp"
  source_port_range = "*"
  destination_port_range = "2376"
  source_address_prefixes = ["${data.terraform_remote_state.env-test.test-nat-gateway-ip}/32"]
  destination_address_prefix = "*"
  resource_group_name = "${azurerm_resource_group.test.name}"
  network_security_group_name = "${azurerm_network_security_group.public.name}"
}

# Verify the changes that Terraform will perform
./gradlew tfplan

# Commit the changes and apply them
git add -u
git commit -m 'Allow AWS Test Jenkins to deploy to Docker swarm'
./gradlew tfapply
git log -3

# Test that the connection is now open
ssh ubuntu@jenkins-test
export DOCKER_HOST=tcp://docker-test-a.northeurope.cloudapp.azure.com:2376
export DOCKER_TLS_VERIFY=1
export DOCKER_CERT_PATH=./docker-a-certs
docker nodels
exit

https://asciinema.org/connect/cc1cb3b8-790e-426a-8e3d-bcb74a9825b1
https://asciinema.org/a/n0kkVhseBCAtsaf9gvlzyDrHv