# DS DevOps Terraform demo

Here we show how to create a firewall (security group) rule that allows the Jenkins build server on Amazon Web Services to make connections to the Docker Swarm running in Azure.

## The problem

Here is a simple overview of the underlying architecture that facilitates container deployment into Amazon Web Services:

![architectural overview](architecture.png)

The problem is that the Jenkins build server on AWS is not allowed to make connections to the Docker Swarm on Azure.

The access to the Swarm is controlled by the subnet network security group, which at the moment only allows connections from the gateway subnet, the swarm itself, and a small handful of IPs within DS (only via SSH):

![tight access rules](azure-missing-rule.png)

What we want is to add a new rule to the security group such that it will accept connections from Jenkins build server to the swarm in Azure.  Jenkins is on a private subnet on AWS, so it has to go through the Network Address Translation gateway on AWS:

![NAT gateway](aws-test-nat-ip.png)

The Terraform code that defines the security group rules look like this:

![terraform code before new rule](terraform-before-rule.png)

## The solution

This is how we carry out such a change:

[![asciicast](https://asciinema.org/a/188519.png)](https://asciinema.org/a/188519)

After the change, the new rule is codified by Terraform:

![terraform code with jenkins rule](terraform-after-rule.png)

After the change has been verified to work and committed to the repository, the Azure network security group has the extra rule:

![access rules for jenkins](azure-rule-for-jenkins.png)
